FROM ruby:2.5.1

ENV BUNDLER_VERSION=2.0.1

RUN \
  apt-get update -qy && \
  apt-get install -y nodejs && \
  gem install bundler --no-document -v 2.0.1
